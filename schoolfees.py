import tkinter as tk
from tkinter import messagebox
from PIL import ImageTk

class SchoolFeesPage(tk.Tk):
    def _init_(self):
        super()._init_()
        #print("hello")
        self.title("School Fees Management System")
        self.geometry("600x400")

        self.tuition_fee = 1000
        self.uniform_fee = 200
        self.book_fee = 300

        self.background_image = ImageTk.PhotoImage(file="Frontend/images/school_bus.jpg")  
        self.create_widgets()

    def create_widgets(self):
        background_label = tk.Label(image=self.background_image)
        background_label.place(x=0, y=0, relwidth=1, relheight=1)

        tk.Label(self, text="School Fees", font=("Helvetica", 24), bg="white").pack(pady=10)

        self.tuition_var = tk.IntVar()
        self.uniform_var = tk.IntVar()
        self.book_var = tk.IntVar()

        tk.Label(self, text="Tuition Fee: Rs1000", bg="lightskyblue",borderwidth=10).pack(padx=5, pady=5)
        tk.Entry(self, textvariable=self.tuition_var,borderwidth=2,relief="solid").pack(padx=5,pady=5)

        tk.Label(self, text="Uniform Fee: Rs200", bg="lightskyblue",borderwidth=10).pack(padx=5,pady=5)
        tk.Entry(self, textvariable=self.uniform_var,borderwidth=2,relief="solid").pack(padx=5,pady=5)

        tk.Label(self, text="Book Fee: Rs300", bg="lightskyblue",borderwidth=10).pack(padx=5,pady=5)
        tk.Entry(self, textvariable=self.book_var,borderwidth=2,relief="solid").pack(padx=5,pady=5)

        tk.Button(self, text="Calculate Total", command=self.calculate_total, bg="lightblue").pack(pady=10)
        tk.Button(self, text="Generate Receipt", command=self.generate_receipt, bg="lightgreen").pack()

    def calculate_total(self):
        tuition = self.tuition_var.get() * self.tuition_fee
        uniform = self.uniform_var.get() * self.uniform_fee
        books = self.book_var.get() * self.book_fee

        total = tuition + uniform + books

        messagebox.showinfo("Total Fees", f"Total Fees: ${total}")

    def generate_receipt(self):
        tuition = self.tuition_var.get() * self.tuition_fee
        uniform = self.uniform_var.get() * self.uniform_fee
        books = self.book_var.get() * self.book_fee

        total = tuition + uniform + books

        receipt_text = f"Tuition Fee: ${tuition}\nUniform Fee: ${uniform}\nBook Fee: ${books}\nTotal: ${total}"

        messagebox.showinfo("Receipt", receipt_text)

if __name__ == '_main_':
    print("hello")
    SchoolFeesPage.mainloop()
