import sys
from PyQt5.QtWidgets import QWidget , QApplication ,QVBoxLayout,QLabel,QPushButton,QComboBox,QRadioButton,QSlider
from PyQt5.QtCore import Qt 
from PyQt5.QtGui import QColor

class MainWindow(QWidget):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Main Window")
        self.setGeometry(500 ,300 ,500 ,300)
        self.mainLayout = QVBoxLayout(self)
        self.mainLayout.setSpacing(0)
        self.setLayout(self.mainLayout)

        self.label1 = QLabel("Hello core2Web")
        self.label1.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.label1.setFixedSize(500,30)
        self.mainLayout.addWidget(self.label1,alignment=Qt.AlignmentFlag.AlignCenter)

        self.slider = QSlider(Qt.Horizontal, self)
        self.mainLayout.addWidget(self.slider)

        self.label = QLabel("Slider Value: 0",self)
        self.mainLayout.addWidget(self.label)

        self.slider.valueChanged.connect(self.update_label)

        self.setLayout(self.mainLayout)


        self.addButton()
        self.init_ui()
        self.radioui()

    def update_label(self):
        value = self.slider.value()
        self.label.setText(f"Slider Value : {value}")



    def addButton(self):
        self.Button1 = QPushButton("Text Change")
        self.Button1.setFixedSize(100,30)
        self.Button1.clicked.connect(lambda:self.label1.setText("Hello Core2Web" if self.label1.text()== " I am Here" else "I am Here"))
        self.mainLayout.addWidget(self.Button1,alignment=Qt.AlignmentFlag.AlignCenter)

    def init_ui(self):

        #layout = QVBoxLayout()

        color_combo = QComboBox(self)

        colors = ['Red','Green','Blue','Yellow','Cyan','Magenta']
        color_combo.addItems(colors)

        color_combo.currentIndexChanged.connect(self.change_color)

        self.mainLayout.addWidget(color_combo)
        

        self.setLayout(self.mainLayout)

        self.change_color(0)

        self.setWindowTitle('color changer app')

    def change_color(self,index):

        color_name = self.sender().currentText() if self.sender() else "Red"

        color = QColor(color_name)

        self.setStyleSheet(f"background-color: {color.name()};")

    def radioui(self):

        self.setWindowTitle("Radio Button Example")

        self.option1_radio = QRadioButton("Python",self)
        self.option2_radio = QRadioButton("Java",self)
        self.option3_radio = QRadioButton("C++",self)

        self.mainLayout.addWidget(self.option1_radio)
        self.mainLayout.addWidget(self.option2_radio)
        self.mainLayout.addWidget(self.option3_radio)

        self.selected_option_label = QLabel("Selected Option: None",self)
        self.mainLayout.addWidget(self.selected_option_label)

        self.option1_radio.toggled.connect(lambda: self.update_selected_option("Python"))
        self.option2_radio.toggled.connect(lambda: self.update_selected_option("Java"))
        self.option3_radio.toggled.connect(lambda: self.update_selected_option("C++"))

        self.setLayout(self.mainLayout)

    def update_selected_option(self,option):

        if option:
            self.selected_option_label.setText(f"Selected Option: {option}")

        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_Window=MainWindow()
    main_Window.show()
    sys.exit(app.exec_())


