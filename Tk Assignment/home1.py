import tkinter as tk
import os
from tkinter import ttk, messagebox,Button


class Assignment:

    def __init__(self):

        # initializer the root of the tkinter

        self.c2w_root = tk.Tk()
        self.c2w_screen_width = self.c2w_root.winfo_screenwidth()
        self.c2w_screen_height = self.c2w_root.winfo_screenheight()
        self.c2w_root.title("Card Generator")

        self.c2w_root.geometry(f"{self.c2w_screen_width}x{self.c2w_screen_height}")

        self.label()

    def label(self):

        #-------full name---------
        self.name = tk.Label(text="Full name").grid(row=0, column=0)
        #self.name.pack()
        #self.name.grid(row=0)

        #-----label-------
        self.name_input = tk.Entry().grid(row=0, column=1)
        #self.name_input.pack()

        #self.name_input.grid(row=0, column=1)

        #------Age--------
        self.age = tk.Label(text="Age : ").grid(row=1, column=0)
        #self.age.pack()

        #------label--------
        self.age_input = tk.Entry().grid(row=1, column=1)
        #self.age_input.pack()

        #--------Email--------
        self.email = tk.Label(text="Email : ").grid(row=2, column=0)
        #self.email.pack()

        #------label--------
        self.email_input = tk.Entry().grid(row=2, column=1)
        #self.email_input.pack()

        ttk.Checkbutton(text='Do You Work').grid()

        #-------- Comp name-------
        self.comp_nam = tk.Label(text="Company name : ").grid(row=3, column=0)
       # self.comp_nam.pack()

        #--------- label -------
        self.comp_input = tk.Entry().grid(row=3, column=1)
       # self.comp_input.pack()

        #----------- package ---------
        self.package = tk.Label(text="Package : ").grid(row=4, column=0)
       # self.package.pack()

        #---------- label ----------

        self.pack_input = tk.Entry().grid(row=4, column=1)
        #self.pack_input.pack(pady=10)

        self.btn = Button(text = 'Generated Card',background="green", padx=10, pady=10, command=self.generated).grid(rows=2, column=2)
        #self.btn.pack()

    def generated(self):

        self.frame = tk.Frame(self.c2w_root,width=300, height=400,bg="green")
        self.frame.grid(sticky="ne",pady=2)
        #self.frame.(width=200, height=200)

        self.Name = tk.Label(self.frame,text="First name : "+self.name_input.get(),bg="green")
        self.Name.config(fg="white")
        self.Name.grid()

        self.Age = tk.Label(self.frame, text="Age :"+ self.age_input.get(),bg="green")
        self.Age.config(fg="white")
        self.Age.grid()

        self.Email = tk.Label(self.frame, text="Email :"+ self.email_input.get(),bg="green")
        self.Email.config(fg="white")
        self.Email.grid()

        self.Comp = tk.Label(self.frame, text="Comp :"+ self.comp_input.get(),bg="green")
        self.Comp.config(fg="white")
        self.Comp.grid()

        self.Package = tk.Label(self.frame , text="Package :"+ self.pack_input.get(),bg="green")
        self.Package.config(fg="white")
        self.Package.grid()
        
        
    def run(self):
        self.c2w_root.mainloop()
