import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg

class MainWindow(qtw.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello Python")
        self.setGeometry(500,300,500,500)
        self.setLayout(qtw.QVBoxLayout())

        my_label = qtw.QLabel("hello")
        my_label.setGeometry(30,30,30,30)
        self.layout().addWidget(my_label)

        my_entry = qtw.QLineEdit()
        my_entry.setObjectName("Name-Field")
        my_entry.setText("")
        self.layout().addWidget(my_entry)

        my_button = qtw.QPushButton("Press", clicked = lambda: press_it())
        self.layout().addWidget(my_button)
    

        self.show()

        def press_it():
            my_label.setText(f'hello {my_entry.text()}')
            my_entry.setText("")

        

app = qtw.QApplication([])
mw = MainWindow()
app.exec_()
