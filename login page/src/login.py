import tkinter as tk 
from tkinter import PhotoImage,Image,ttk
from PIL import ImageTk, Image

def validate_login(username, password):
    print("Username entered:", username.get())
    print("Password entered:", password.get())
    # Add your business logic here to validate the credentials

def center(win):
    win.update_idletasks()
    width = win.winfo_width()
    frm_width = win.winfo_rootx() - win.winfo_x()
    win_width = width + 2 * frm_width
    height = win.winfo_height()
    titlebar_height = win.winfo_rooty() - win.winfo_y()
    win_height = height + titlebar_height + frm_width
    x = win.winfo_screenwidth() // 2 - win_width // 2
    y = win.winfo_screenheight() // 2 - win_height // 2
    win.geometry(f"{width}x{height}+{x}+{y}")
    win.deiconify()

root = tk.Tk()
root.title("Creative Login Form")
root.geometry('400x600')
entry = tk.Entry(root, width=40, font=('Helvetica', 12))
#entry.insert(0, 'Enter Text:- ')  # Initial placeholder text
entry.config(fg='grey50')
entry.pack(pady=60)





# Load a background image
bg_image = ImageTk.PhotoImage(file="assets/cosm.jpg")
#bg_image.(x=0,y=0,relwidth=1,relheight=1)
bg_label = tk.Label(root, image=bg_image)
bg_label.place(x=5, y=5, relwidth=1, relheight=1)

style = ttk.Style()
style.theme_use("default")
style.configure("Capsule.TLabel", borderwidth=2, relief="solid", font=("fantasy", 12))

login_label = ttk.Label(root,text="Login Page", border=10,font="fantasy",justify= ['center'],width=20,background="lightblue",anchor="center")
login_label.pack(padx=10,pady=10)




# Username label and entry
username_label = ttk.Label(root, text="Username", font=("fantasy", 12,"bold"), borderwidth=15, background="lightpink")
username_label.pack(pady=5,padx=5)
username = tk.StringVar()
username_entry = tk.Entry(root, textvariable=username, borderwidth=3,background="lightpink")
username_entry.pack(pady=15)
username_entry = ttk.Entry(root, style="Rounded.TEntry")

# Password label and entry
password_label = ttk.Label(root, text="Password", font=("fantasy", 12,"bold"), borderwidth=15,background="lightpink")
password_label.pack(pady=5,padx=5)
password = tk.StringVar()
password_entry = tk.Entry(root, textvariable=password, show="*", borderwidth=3,background="white")
password_entry.pack(pady=15)
password_entry = ttk.Entry(root, style="Rounded.TEntry", show="*")

# Login button
login_button = ttk.Button(root, text="Login",style="Rounded.TButton" ,command=lambda: validate_login(username, password))

login_button.pack()

# Center the window
center(root)

root.mainloop()
