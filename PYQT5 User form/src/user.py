from PyQt5.QtWidgets import *
import sys


class Window(QDialog):

    def __init__(self):
        
        super(Window,self).__init__()

        #created window title
        self.setWindowTitle("User Form")

        #created tab geometry
        self.setGeometry(500,500,500,500)

        self.formGroupBox = QGroupBox("User Profile Information")

        #self.formGroupBox.setStyleSheet('QGroupBox:title {color: rgb(1, 130, 153);}')

        self.formGroupBox.setStyleSheet('QGroupBox  {background-color: green;}')

        self.nameLineEdit1 = QLineEdit()
        self.nameLineEdit2 = QLineEdit()
        self.nameLineEdit3 = QLineEdit()
        self.nameLineEdit4 = QLineEdit()
        self.nameLineEdit5 = QLineEdit()



        self.createForm()

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)

        self.buttonBox.accepted.connect(self.getInfo)

        self.mainLayout = QVBoxLayout()

        self.mainLayout.addWidget(self.formGroupBox)

        self.mainLayout.addWidget(self.buttonBox)

        self.setLayout(self.mainLayout)

    def getInfo(self):

        print("First Name : {0}".format(self.nameLineEdit1.text()))

        print("Last Name : {0}".format(self.nameLineEdit2.text()))

        print("Mobile No : {0}".format(self.nameLineEdit3.text()))

        #print("Gender : {0}".format(self.nameLineEdit4.text()))

        print("Height : {0}".format(self.nameLineEdit5.text()))

    def createForm(self):

        #self.mainLayout = QVBoxLayout()

        self.Layout = QFormLayout()

        self.Layout.addRow(QLabel("First Name : "),self.nameLineEdit1)

        self.Layout.addRow(QLabel("Last Name : "),self.nameLineEdit2)

        self.Layout.addRow(QLabel("Mobile No : "),self.nameLineEdit3)

        #self.formGroupBox.setLayout(self.Layout)
        
        self.Layout.addRow(QLabel("Gender : "),self.nameLineEdit4)

        radio1 = QRadioButton("Male")
        radio2 = QRadioButton("Female")

        radio1.setChecked(True)
        radio2.setChecked(True)

        #vbox = QVBoxLayout()

        self.Layout.addWidget(radio1)
        self.Layout.addWidget(radio2)

        self.Layout.addRow(QLabel("Height : "),self.nameLineEdit5)

        self.fileName = QFileDialog.getOpenFileName(self,("Open Image"), "/home/Desktop",("Image Files (*.png *.jpg *.bmp)"))


        self.formGroupBox.setLayout(self.Layout)

        #self.Layout.addStretch(1)
        


if __name__ == "__main__":

    #create pyqt5 app
    app = QApplication(sys.argv)

    #create instance of our window
    window = Window()

    #showing the window
    window.show()

    #start the app
    sys.exit(app.exec())

