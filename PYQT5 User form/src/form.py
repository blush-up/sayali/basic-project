from PyQt5.QtWidgets import *
import sys


class Window(QDialog):

    def __init__(self):
        
        super(Window,self).__init__()

        #created window title
        self.setWindowTitle("User Form")

        #created tab geometry
        self.setGeometry(100,100,300,400)

        self.formGroupBox = QGroupBox("form 1")

        self.ageSpinBar = QSpinBox()

        self.degreeComboBox = QComboBox()

        self.degreeComboBox.addItems(["Btech","Mtech","BE","PHD"])

        self.nameLineEdit = QLineEdit()

        self.createForm()

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok)

        self.buttonBox.accepted.connect(self.getInfo)

        mainLayout = QVBoxLayout()

        mainLayout.addWidget(self.formGroupBox)

        mainLayout.addWidget(self.buttonBox)

        self.setLayout(mainLayout)

    def getInfo(self):

        print("Person Name : {0}".format(self.nameLineEdit.text()))

        print("Degree : {0}".format(self.degreeComboBox.currentText()))

        print("Age : {0}".format(self.ageSpinBar.text()))

        self.close()

    def createForm(self):

        layout = QFormLayout()

        layout.addRow(QLabel("Name : "),self.nameLineEdit)

        layout.addRow(QLabel("Degree : "),self.degreeComboBox)

        layout.addRow(QLabel("Age : "),self.ageSpinBar)

        self.formGroupBox.setLayout(layout)


if __name__ == "__main__":

    #create pyqt5 app
    app = QApplication(sys.argv)

    #create instance of our window
    window = Window()

    #showing the window
    window.show()

    #start the app
    sys.exit(app.exec())


